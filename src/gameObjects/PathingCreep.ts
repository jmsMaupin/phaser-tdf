export default class PathingCreep extends Phaser.Sprite {
	goal: Phaser.Point;
	waypointIndex: number = 0;
	path: Phaser.Point[]; 
	constructor(game: Phaser.Game, key: string, path: Phaser.Point[]){
		super(game, path[0].x, path[0].y, key)
		this.frame = this.getFrame(path[0], path[1]);
		this.path = path;

		this.animations.add('walk-up', [1, 2]);
		this.animations.add('walk-down', [4, 5]);
		this.animations.add('walk-right', [10, 11]);
        this.animations.add('walk-left',  [7, 8]);

        this.anchor.setTo(0, 1)

		game.physics.enable(this, Phaser.Physics.ARCADE)
	}

	getFrame(point1: Phaser.Point, point2: Phaser.Point) : number {
		let direction = Phaser.Point.subtract(point2,point1)
		// console.log(direction)
		if(Math.abs(point2.x - point1.x) < 6)
			if(direction.y < 0)
				return 0;
			else
				return 3;
		else
			if(direction.x < 0)
				return 6;
			else
				return 9;
	}

	spawn() {
		this.reset(this.path[0].x, this.path[0].y)
		this.waypointIndex = 0;
		this.setGoal(this.path[1]);
	}

	getNextWaypoint() {
		this.waypointIndex++;
		return this.path[this.waypointIndex]
	}

	setGoal(waypoint: Phaser.Point) {
		this.goal = waypoint;
	}

	derender() {
		this.kill();
	}

	getDirection(origin: Phaser.Point, destination: Phaser.Point) {
       return Phaser.Point.normalize(Phaser.Point.subtract(destination, origin));
    }

	update() {
		if(this.goal){
			let frameRef: number = this.getFrame(this.world, this.goal);

			if(Phaser.Point.distance(this.goal, this.world) < 5) {
				if(this.path[this.path.length - 1] == this.goal)
					this.derender();
				else
					this.setGoal(this.getNextWaypoint());
			} else {
				this.body.velocity = 
					this.getDirection(this.world, this.goal)
						.setMagnitude(100);
			}

			if (frameRef === 0)
				this.animations.play('walk-up', 6);
			else if (frameRef === 3)
				this.animations.play('walk-down', 6);
			else if (frameRef === 6)
				this.animations.play('walk-left', 6);
			else
				this.animations.play('walk-right', 6);
		}
	}


}