export default class Player extends Phaser.Sprite {
    goal: Phaser.Point;
    origin: Phaser.Point;
    facingRight: Boolean;

    constructor(game: Phaser.Game, origin: Phaser.Point, key: string, frame: number) {

        super(game, origin.x, origin.y, key, frame);
        this.origin = origin;
        this.anchor.setTo(0, 1);
        this.animations.add('walk-right', [10, 11]);
        this.animations.add('walk-left',  [7, 8]);

        game.physics.enable(this, Phaser.Physics.ARCADE)
        game.add.existing(this);

    }

    update() {
        if(this.body.velocity.x > 0){
          this.facingRight = true;
          this.animations.play('walk-right', 6);
        }
        else if (this.body.velocity.x < 0){
          this.facingRight = false;
          this.animations.play('walk-left', 6);
        }
        else{
          this.animations.stop();
          this.animations.frame = this.facingRight ? 9 : 6;
        }

        if(this.goal){
          if(Phaser.Point.distance(this.goal, this.world) < 5){
             this.derender();
          }
          else {
            this.body.velocity = 
              this.getDirection(this.world, this.goal)
              .setMagnitude(60);
          }
        }
    }

    derender() {
      this.kill();
    }

    setGoal(location: Phaser.Point) {
        this.goal = location;
    }

    getDirection(origin: Phaser.Point, destination: Phaser.Point) {
        return Phaser.Point.normalize(Phaser.Point.subtract(destination, origin));
    }

    spawn(goal: Phaser.Point) {
      this.reset(this.origin.x, this.origin.y)
      this.setGoal(goal);
    }


}