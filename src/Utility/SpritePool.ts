import Creep from '../gameObjects/Creep';
export default class SpritePool extends Phaser.Group {
	origin: Phaser.Point;

	constructor(game: Phaser.Game, instanceMax: number, name: string, origin: Phaser.Point) {
		super(game, game.world, name);
		this.game = game;
		this.origin = origin;

		if(instanceMax > 0) {
			for(let c = 0; c < instanceMax; ++c) {
				let creep = new Creep(this.game, this.origin, 'creep', 9);
				creep.kill();
				this.add(creep);
			}
		}

		return this;
	}

	spawn(goal: Phaser.Point) {
		let creep = this.getFirstExists(false);
		if(!creep){
			creep = new Creep(this.game, this.origin, 'creep', 9);
			this.add(creep);
		}

		console.log(this.length)
		creep.spawn(goal);
	}
}