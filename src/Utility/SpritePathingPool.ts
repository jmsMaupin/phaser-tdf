import Creep from '../gameObjects/PathingCreep';
export default class SpritePool extends Phaser.Group {

	path: Phaser.Point[];

	constructor(game: Phaser.Game, instanceMax: number, name: string, path: Phaser.Point[]) {
		super(game, game.world, name);
		this.game = game;
		this.path = path;

		if(instanceMax > 0) {
			for(let c = 0; c < instanceMax; ++c) {
				let creep = new Creep(this.game, 'creep', path);
				creep.kill();
				this.add(creep);
			}
		}

		return this;
	}

	spawn() {
		let creep = this.getFirstExists(false);
		if(!creep){
			creep = new Creep(this.game, 'creep', this.path);
			this.add(creep);
		}

		creep.spawn();
	}
}