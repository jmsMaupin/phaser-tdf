import Creep from '../gameObjects/Creep';
import PathingCreep from '../gameObjects/PathingCreep';
import SpritePool from '../Utility/SpritePathingPool';

export default class InitialGameState extends Phaser.State {
	map: Phaser.Tilemap;
	layer: Phaser.TilemapLayer;
	playerStart: Phaser.Point;
	playerEnd: Phaser.Point;
	creepPool: SpritePool;
	creep: PathingCreep;
	gfx: Phaser.Graphics;

	create() {
		this.setupMap();
		let path = this.getAllObjectsOfType('waypoint', this.map, 'pathfinding');
		this.creepPool = new SpritePool(this.game, 1, 'creepspawner', path);
		this.time.events.repeat(Phaser.Timer.SECOND, Infinity, this.spawnCreeps.bind(this));
		this.game.input.addMoveCallback(this.move, this.game);
		this.game.input.mouse.capture = true;
		this.drawSquare(0,0);
		
	}

	update() {
		let pointer = this.game.input.mousePointer;
		this.updateSquare(
			Math.floor(pointer.x / 32) * 32, 
			Math.floor(pointer.y / 32) * 32);
	}

	drawSquare(x, y) {
		this.gfx = this.game.add.graphics(0, 0);
		this.gfx.beginFill(0x0000ff, .2);
		this.gfx.lineStyle(2, 0x0000FF, .2);
		this.gfx.drawRect(x, y, 32, 32);
	}

	updateSquare(x, y) {
		this.gfx.x = x;
		this.gfx.y = y;
	}

	move() {
		console.log('moved')
	}

	spawnCreeps() {
		this.creepPool.spawn();
	}

	getCreepPath() {
		console.log(this.getAllObjectsOfType('waypoint', this.map, 'pathfinding'))
	}

	setupMap() {
		this.map = this.add.tilemap('complicated');
		this.map.addTilesetImage('wooded_tiles', 'wood_tileset');

		this.layer = this.map.createLayer('tiles');
		this.layer.resizeWorld();
	}

	getAllObjectsOfType(type: string, map: Phaser.Tilemap, objectLayer: string) {
		return map.objects[objectLayer].filter( (element) => {
			return element.type === type;
		});
	}

	getFirstObjectByType(type: string, map: Phaser.Tilemap, objectLayer: string) {
		var ele = undefined;
		map.objects[objectLayer].forEach( (element) => {
			if (element.type == type) {
				ele = element;
			}
		})

		return ele;
	}

	getObjectByName(name: string, map: Phaser.Tilemap, objectLayer: string) {
		var ele = undefined;
		map.objects[objectLayer].forEach( (element) => {
			if(element.name == name) {
				ele = element;
			}
		})

		return ele;
	}
}